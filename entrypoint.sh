#!/bin/bash

echo "Start flush db"
python manage.py flush --no-input --settings=dbTest.settings.docker_settings
python manage.py flush --no-input --database mysql --settings=dbTest.settings.docker_settings
python manage.py flush --no-input --database mongo --settings=dbTest.settings.docker_settings
echo "End flush db"


echo "Start postgres"
python manage.py migrate --settings=dbTest.settings.docker_settings.postgres_settings
python manage.py fake_data $FAKE_BATCH_SIZE --settings=dbTest.settings.docker_settings.postgres_settings
python manage.py dumpdata -o fake_data.json --exclude=auth --exclude=contenttypes --settings=dbTest.settings.docker_settings.postgres_settings
python manage.py fake_db_row --settings=dbTest.settings.docker_settings.postgres_settings
echo "End postgres"


echo "Start mysql"
python manage.py migrate --settings=dbTest.settings.docker_settings.mysql_settings
python manage.py loaddata fake_data.json --settings=dbTest.settings.docker_settings.mysql_settings
python manage.py fake_db_row --settings=dbTest.settings.docker_settings.mysql_settings
echo "End mysql"


echo "Start mongo"
python manage.py loaddata fake_data.json --settings=dbTest.settings.docker_settings.mongo_settings
python manage.py fake_db_row --settings=dbTest.settings.docker_settings.mongo_settings
echo "End mongo"

python manage.py runserver 0.0.0.0:7000 --settings=dbTest.settings.docker_settings