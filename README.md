# dbTest

Synthetic "performance" database (postgresql, mysql, mongodb) tests.

`docker-compose up` for run project and check `http://0.0.0.0:7000/api/`.

In `docker-compose.yaml` you can change env variable FAKE_BATCH_SIZE (default=100).