from datetime import datetime

from django.core.management.base import BaseCommand

from some_api.views import SomeFactory


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('batch_size', type=int)

    def handle(self, *args, **options):
        start = datetime.now()
        print(f'start: {start}')

        size = options.get('batch_size', 100)
        SomeFactory.create_batch(size=size)

        end = datetime.now()
        print(f'Generated fake data: {end - start} (size={size})')
