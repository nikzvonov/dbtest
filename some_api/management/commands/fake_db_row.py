from datetime import datetime

from django.conf import settings
from django.core.management.base import BaseCommand

from some_api.views import SomeFactory, SomeModel


def mongo_data():
    return {
        'id': SomeModel.objects.count() + 1,
        'release_at': datetime.now(),
        'is_active': False,
        'name': 'Mongo DataBase',
        'description': 'MongoDB is a source-available cross-platform document-oriented database program.',
        'email': 'mongo@db.com',
        'address': 'https://www.mongodb.com/',
        'image_url': 'https://upload.wikimedia.org/wikipedia/en/4/45/MongoDB-Logo.svg',

        'point_id': 51,
        'count': 1,
        'rating': 5,
        'temperature': 38.5,
        'some_type': 1,
    }


def mysql_data():
    return {
        'release_at': datetime.now(),
        'is_active': False,
        'name': 'MySQL DataBase',
        'description': 'MySQL is an open-source relational database management system (RDBMS).',
        'email': 'my@sql.db',
        'address': 'https://www.mysql.com/',
        'image_url': 'https://upload.wikimedia.org/wikipedia/en/d/dd/MySQL_logo.svg',

        'point_id': 51,
        'count': 1,
        'rating': 1,
        'temperature': 39.9,
        'some_type': 0,
    }


def postgresql_data():
    return {
        'release_at': datetime.now(),
        'is_active': True,
        'name': 'Postgre SQL DataBase',
        'description': 'PostgreSQL - свободная объектно-реляционная система управления базами данных.',
        'email': 'postgre@sql.db',
        'address': 'https://www.postgresql.org/',
        'image_url': 'https://www.postgresql.org/media/img/about/press/elephant.png',

        'point_id': 51,
        'count': 1,
        'rating': 10,
        'temperature': 36.6,
        'some_type': 2,
    }


class Command(BaseCommand):

    def handle(self, *args, **options):
        current_database = settings.DATABASES.get('default', {})
        if current_database == settings.POSTGRES:
            SomeFactory.create(**postgresql_data())
        elif current_database == settings.MYSQL:
            SomeFactory.create(**mysql_data())
        elif current_database == settings.MONGO:
            SomeFactory.create(**mongo_data())
