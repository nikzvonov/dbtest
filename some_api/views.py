import factory
from rest_framework import viewsets, serializers

from some_api.models import SomeModel


class SomeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SomeModel

    release_at = factory.Faker('date_this_year')
    is_active = factory.Faker('boolean')

    name = factory.Faker('name')
    description = factory.Faker('text')
    email = factory.Faker('email')
    address = factory.Faker('address')
    image_url = factory.Faker('uri')

    point_id = factory.Faker('pyint', min_value=1, max_value=50)
    count = factory.Faker('pydecimal', left_digits=8, right_digits=3, positive=None)
    rating = factory.Faker('pyint')
    temperature = factory.Faker('pyfloat')
    some_type = factory.Faker('pyint', min_value=0, max_value=2)


class SomeSerializer(serializers.ModelSerializer):

    class Meta:
        model = SomeModel
        fields = '__all__'


class SomeViewSet(viewsets.ModelViewSet):
    serializer_class = SomeSerializer
    filterset_fields = '__all__'
    search_fields = ['name', 'description', 'email', 'address']


class PostgreSQLViewSet(SomeViewSet):
    queryset = SomeModel.objects.all().order_by('id')


class MySQLViewSet(SomeViewSet):
    queryset = SomeModel.objects.using('mysql').all().order_by('id')


class MongoViewSet(SomeViewSet):
    queryset = SomeModel.objects.using('mongo').all().order_by('id')
