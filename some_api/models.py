from django.db import models


class SomeModel(models.Model):

    class Type(models.IntegerChoices):
        BAD = 0
        NORMAL = 1
        GOOD = 2

    release_at = models.DateTimeField()
    is_active = models.BooleanField()

    name = models.CharField(max_length=50)
    description = models.TextField()
    email = models.EmailField()
    address = models.CharField(max_length=500)
    image_url = models.URLField()

    point_id = models.PositiveBigIntegerField()
    count = models.DecimalField(max_digits=12, decimal_places=3)
    rating = models.IntegerField()
    temperature = models.FloatField()
    some_type = models.PositiveSmallIntegerField(choices=Type.choices)
