from dbTest.settings import *


MYSQL['HOST'] = 'mysql_db'
MYSQL['PORT'] = '3307'
POSTGRES['HOST'] = 'postgresql_db'

DATABASES = {
    'default': POSTGRES,
    'mysql': MYSQL,
    'mongo': MONGO,
}
