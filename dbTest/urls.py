from django.conf.urls import include
from django.urls import path

from rest_framework.routers import DefaultRouter

from some_api import views


api_router = DefaultRouter()
api_router.register('some-mysql-data', views.MySQLViewSet, 'some_mysql_data')
api_router.register('some-postgresql-data', views.PostgreSQLViewSet, 'some_postgresql_data')
api_router.register('some-mongo-data', views.MongoViewSet, 'some_mongo_data')

urlpatterns = [
    path('api/', include(api_router.urls)),
]
